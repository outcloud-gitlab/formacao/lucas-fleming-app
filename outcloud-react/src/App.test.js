import { render, screen } from '@testing-library/react';
import App from './App';

test('renders saiba mais link', () => {
  render(<App />);
  const linkElement = screen.getByText(/saiba mais/i);
  expect(linkElement).toBeInTheDocument();
});
